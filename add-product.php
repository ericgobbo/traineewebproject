<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Add product</title>
    <link href='https://fonts.googleapis.com/css?family=Lexend' rel='stylesheet'>
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <div class="container">
        <div class="topbar">

            <div>
                <h2>Product Add</h2>
            </div>
            <div style="display:flex; align-items: center; gap: 20px;">
                <button onclick="submitForm()" class="button">Save</button>
                <button id="delete-product-btn" class="button danger"
                    onclick="location.href='index.php'">Cancel</button>
            </div>
        </div>
        <div>
            <form id="product_form">
                <label for="sku">SKU <span class="text_danger">*</span></label>
                <input type="text" id="sku" name="sku" placeholder="Product SKU">

                <label for="name">Name <span class="text_danger">*</span></label>
                <input type="text" id="name" name="name" placeholder="Product name">

                <label for="price">Price <span class="text_danger">*</span></label>
                <input type="text" id="price" name="price" placeholder="Product price">

                <label for="productType">Type Switcher <span class="text_danger">*</span></label>
                <select id="productType" name="productType" onChange="showType()">
                    <option value="">Type Switcher</option>
                    <option value="dvd">DVD</option>
                    <option value="furniture">Furniture</option>
                    <option value="book">Book</option>
                </select>
                <div class="hide" id="dvd">
                    <div class="information">Please, provide size</div>
                    <label for="size">Size (MB)</label>
                    <input type="text" id="size" name="size" placeholder="Product size">
                </div>
                <div class="hide" id="furniture">
                    <div class="information">Please, provide dimensions in HxWxL format</div>
                    <label for="height">Height (CM)</label>
                    <input type="text" id="height" name="height" placeholder="Product height">
                    <label for="width">Width (CM)</label>
                    <input type="text" id="width" name="width" placeholder="Product width">
                    <label for="lenght">Lenght (CM)</label>
                    <input type="text" id="length" name="length" placeholder="Product length">
                </div>
                <div class="hide" id="book">
                    <div class="information">Please, provide weight</div>
                    <label for="weight">Weight (KG)</label>
                    <input type="text" id="weight" name="weight" placeholder="Product weight">
                </div>
            </form>
        </div>
        <footer class="footer">Scandiweb Test assignment</footer>
    </div>
    <script type="text/javascript">
    let selectedType = "";

    function showType() {
        if (selectedType != "") {
            document.getElementById(selectedType).classList.add("hide");
        }
        let t = document.getElementById("productType").value;
        if (t != "") {
            document.getElementById(t).classList.remove("hide");
            selectedType = t;
        }
    }
    var isNumber = function isNumber(value) {
        return typeof value === 'number' && isFinite(value);
    }

    function submitForm() {
        var form = document.getElementById('product_form');
        var data = new FormData(form);
        var obj = {};
        for (var key of data.keys()) {
            obj[key] = data.get(key);
        }
       // console.log(obj);

        if (document.getElementById("sku").value == "") {
            alert("Please, submit required data");
            return;
        }
        if (document.getElementById("name").value == "") {
            alert("Please, submit required data");
            return;
        }
        if (document.getElementById("price").value == "") {
            alert("Please, submit required data");
            return;
        }
        if (!isNumber(Number(document.getElementById("price").value))) {
            alert("Price must be a valid number !");
            document.getElementById("price").focus();
            return;
        }
        if (document.getElementById("productType").value == "dvd") {
            if (document.getElementById("size").value == "") {
                alert("Please, submit required data");
                return;
            }
            if (!isNumber(Number(document.getElementById("size").value))) {
                alert("Size must be a valid number !");
                return;
            }

        }
        if (document.getElementById("productType").value == "book") {
            if (document.getElementById("weight").value == "") {
                alert("Please, submit required data");
                return;
            }
            if (!isNumber(Number(document.getElementById("weight").value))) {
                alert("Weight must be a valid number !");
                return;
            }
        }
        if (document.getElementById("productType").value == "furniture") {
            if (document.getElementById("height").value == "") {
                alert("Please, submit required data");
                return;
            }
            if (!isNumber(Number(document.getElementById("height").value))) {
                alert("height must be a valid number !");
                return;
            }
            if (document.getElementById("width").value == "") {
                alert("Please, submit required data");
                return;
            }
            if (!isNumber(Number(document.getElementById("width").value))) {
                alert("width must be a valid number !");
                return;
            }
            if (document.getElementById("length").value == "") {
                alert("Please, submit required data");
                return;
            }
            if (!isNumber(Number(document.getElementById("length").value))) {
                alert("length must be a valid number !");
                return;
            }
        }
        // document.forms["product_form"].submit();
         postData('insert.php', obj)
            .then((data) => {
                if (data.code == 1062) {
                    alert("Sku product already exists !!! Please type another sku.");
                }
                else {
                    if (data.code > 0) {
                        alert("Error insert in DB !")
                    } else {
                        location.href = "../";
                    }
                    
                }
        });
    }

    async function postData(url = '', data = {}) {
        // Default options are marked with *
        const response = await fetch(url, {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            headers: {
                'Content-Type': 'application/json'
                // 'Content-Type': 'application/x-www-form-urlencoded',
            },
            redirect: 'follow', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
            body: JSON.stringify(data) // body data type must match "Content-Type" header
        });
        return response.json(); // parses JSON response into native JavaScript objects
    }
    </script>
</body>

</html>