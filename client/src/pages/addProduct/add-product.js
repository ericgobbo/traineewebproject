const AddProduct=()=>{
    return(
        <div class="container">
        <div class="topbar">

            <div>
                <h2>Product Add</h2>
            </div>
            <div style="display:flex; align-items: center; gap: 20px;">
                <button onclick="submitForm()" class="button">Save</button>
                <button id="delete-product-btn" class="button danger"
                    onclick="location.href='index.php'">Cancel</button>
            </div>
        </div>
        <div>
            <form method="POST" id="product_form" action="insert.php">
                <label for="sku">SKU <span class = "text_danger">*</span></label>
                <input type="text" id="sku" name="sku" placeholder="Product SKU"/>

                <label for="name">Name <span class = "text_danger">*</span></label>
                <input type="text" id="name" name="name" placeholder="Product name">

                <label for="price">Price <span class = "text_danger">*</span></label>
                <input type="text" id="price" name="price" placeholder="Product price">

                <label for="productType">Type Switcher <span class = "text_danger">*</span></label>
                <select id="productType" name="productType" onChange="showType()">
                    <option value="">Type Switcher</option>
                    <option value="dvd">DVD</option>
                    <option value="furniture">Furniture</option>
                    <option value="book">Book</option>
                </select>
                <div class="hide" id="dvd">
                <div class="information">Please, provide size</div>
                    <label for="size">Size (MB)</label>
                    <input type="text" id="size" name="size" placeholder="Product size">
                </div>
                <div class="hide" id="furniture">
                    <div class="information">Please, provide dimensions in HxWxL format</div>
                    <label for="height">Height (CM)</label>
                    <input type="text" id="height" name="height" placeholder="Product height">
                    <label for="width">Width (CM)</label>
                    <input type="text" id="width" name="width" placeholder="Product width">
                    <label for="lenght">Lenght (CM)</label>
                    <input type="text" id="length" name="length" placeholder="Product length">
                <div>
                <div class="hide" id="book">
                <div class="information">Please, provide weight</div>
                    <label for="weight">Weight (KG)</label>
                    <input type="text" id="weight" name="weight" placeholder="Product weight">
                </div>
            </form>
        </div>
        <footer class="footer">Scandiweb Test assignment</footer>
    </div>
    )
}
export default AddProduct;