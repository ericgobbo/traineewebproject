<?php
include 'lib/database.php';
$db = new database();
$db->select("product", "*");
$result = $db->sql;

function asDollars($value) {
    if ($value<0) return "-".asDollars(-$value);
    return '$' . number_format($value, 2);
  }

?>
<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Products page</title>
    <link href='https://fonts.googleapis.com/css?family=Lexend' rel='stylesheet'>
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <div class="container">
        <div class="topbar">

            <div>
                <h2>Product List</h2>
            </div>
            <div style="display:flex; align-items: center; gap: 20px;">
                <a class="button" href="add-product.php" id="add-produt-btn">ADD</a>
                <button class="button danger" id="delete-product-btn" onclick="deleteProd()">MASS DELETE</button>
            </div>
        </div>
        <div class="list">
            <?php 
while($row = mysqli_fetch_assoc($result)){
?>

            <div class="listItem">
                <input type="checkbox" class="delete-checkbox" id="check_<?php echo $row['sku']; ?>"
                    style="margin-left:15px; margin-top: 15px;">
                <div class="listContent">

                    <?php echo $row['sku']; ?><br><?php echo $row['name']; ?><br><?php echo asDollars($row['price'] );?><br>
                    <?php
            if ($row['product_type'] == "dvd"){?>

                    Size: <?php echo $row['size']; ?>MB
                    <?php } ?>

                    <?php
            if ($row['product_type'] == "book"){?>

                    Weight: <?php echo $row['weight']; ?>kg
                    <?php } ?>

                    <?php
            if ($row['product_type'] == "furniture"){?>

                    Dimension:
                    <?php echo $row['dimensionh']; ?>x<?php echo $row['dimensionw']; ?>x<?php echo $row['dimensionl']; ?>
                    <?php } ?>
                </div>
            </div>
            <?php } ?>
        </div>
        <footer class="footer">Scandiweb Test assignment</footer>
    </div>

    <script>
    function deleteProd() {
        let arChecked = [];
        let els = document.getElementsByClassName("delete-checkbox");
        for (let i = 0; i < els.length; i++) {
            if (els[i].checked) {
                arChecked.push(els[i].id.replace('check_', ''));
            }
        }
        if (arChecked.length > 0) {
            // let data = {ids:arChecked};

            fetch("delete.php", {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(arChecked)
            }).then(res => {
                //console.log(res);
                location.href = "index.php";
            });
        }
    }
    </script>
</body>

</html>