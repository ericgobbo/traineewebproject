CREATE TABLE `product` (
  `sku` varchar(100) NOT NULL,
  `name` varchar(150) DEFAULT NULL,
  `price` decimal(10,0) DEFAULT NULL,
  `size` float DEFAULT NULL,
  `weight` float DEFAULT NULL,
  `dimensionh` int(8) DEFAULT NULL,
  `dimensionw` int(8) DEFAULT NULL,
  `dimensionl` int(8) DEFAULT NULL,
  `product_type` varchar(45) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  PRIMARY KEY (`sku`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
