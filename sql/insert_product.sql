/*
-- Query: select * from product
LIMIT 0, 1000

-- Date: 2022-05-04 21:49
*/
INSERT INTO `product` (`sku`,`name`,`price`,`size`,`weight`,`dimensionh`,`dimensionw`,`dimensionl`,`product_type`,`create_date`) VALUES ('GGWP0007','War and Peace',20,NULL,2,NULL,NULL,NULL,'book',NULL);
INSERT INTO `product` (`sku`,`name`,`price`,`size`,`weight`,`dimensionh`,`dimensionw`,`dimensionl`,`product_type`,`create_date`) VALUES ('GGWP0008','War and Peace',20,NULL,2,NULL,NULL,NULL,'book',NULL);
INSERT INTO `product` (`sku`,`name`,`price`,`size`,`weight`,`dimensionh`,`dimensionw`,`dimensionl`,`product_type`,`create_date`) VALUES ('GGWP0009','War and Peace',20,NULL,2,NULL,NULL,NULL,'book',NULL);
INSERT INTO `product` (`sku`,`name`,`price`,`size`,`weight`,`dimensionh`,`dimensionw`,`dimensionl`,`product_type`,`create_date`) VALUES ('GGWP0010','War and Peace',20,NULL,2,NULL,NULL,NULL,'book',NULL);
INSERT INTO `product` (`sku`,`name`,`price`,`size`,`weight`,`dimensionh`,`dimensionw`,`dimensionl`,`product_type`,`create_date`) VALUES ('JVC200120','Acme Disc',1,700,NULL,NULL,NULL,NULL,'dvd',NULL);
INSERT INTO `product` (`sku`,`name`,`price`,`size`,`weight`,`dimensionh`,`dimensionw`,`dimensionl`,`product_type`,`create_date`) VALUES ('JVC200124','Acme Disc',1,700,NULL,NULL,NULL,NULL,'dvd',NULL);
INSERT INTO `product` (`sku`,`name`,`price`,`size`,`weight`,`dimensionh`,`dimensionw`,`dimensionl`,`product_type`,`create_date`) VALUES ('JVC200125','Acme Disc',1,700,NULL,NULL,NULL,NULL,'dvd',NULL);
INSERT INTO `product` (`sku`,`name`,`price`,`size`,`weight`,`dimensionh`,`dimensionw`,`dimensionl`,`product_type`,`create_date`) VALUES ('JVC200126','Acme Disc',1,700,NULL,NULL,NULL,NULL,'dvd',NULL);
INSERT INTO `product` (`sku`,`name`,`price`,`size`,`weight`,`dimensionh`,`dimensionw`,`dimensionl`,`product_type`,`create_date`) VALUES ('TR120555','Chair',40,NULL,NULL,24,45,15,'furniture',NULL);
INSERT INTO `product` (`sku`,`name`,`price`,`size`,`weight`,`dimensionh`,`dimensionw`,`dimensionl`,`product_type`,`create_date`) VALUES ('TR120556','Chair',40,NULL,NULL,24,45,15,'furniture',NULL);
INSERT INTO `product` (`sku`,`name`,`price`,`size`,`weight`,`dimensionh`,`dimensionw`,`dimensionl`,`product_type`,`create_date`) VALUES ('TR120557','Chair',40,NULL,NULL,24,45,15,'furniture',NULL);
INSERT INTO `product` (`sku`,`name`,`price`,`size`,`weight`,`dimensionh`,`dimensionw`,`dimensionl`,`product_type`,`create_date`) VALUES ('TR120558','Chair',40,NULL,NULL,24,45,15,'furniture',NULL);
